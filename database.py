import sqlite3

REVISION = 1

MIGRATIONS = [
    [
        'CREATE TABLE meta (revision integer);',
        'CREATE TABLE project (id text primary_key, title text, pad text, pad_ro text);',
        'CREATE TABLE issue (id integer primary_key, project text, title text, category text, pad text, pad_ro text);',
        'CREATE TABLE issue_file (hash text primary_key, issue integer, user text, title text, mimetype text, uploaded integer);',
        'CREATE TABLE project_file (hash text primary_key, project text, user text, title text, mimetype text, uploaded integer);',
        'CREATE TABLE user (name text primary_key, pwhash text);',
        'CREATE TABLE assignment (user text, issue integer);',
    ]
]

MODELS = {
    'project': ('id', 'title', 'pad', 'pad_ro'),
    'issue': ('id', 'project', 'title', 'category', 'pad', 'pad_ro'),
    'issue_file': ('hash', 'issue', 'user', 'title', 'mimetype', 'uploaded'),
    'project_file': ('hash', 'project', 'user', 'title', 'mimetype', 'uploaded'),
    'user': ('name', 'pwhash'),
}

def init(url):
    conn = sqlite3.connect(url)
    c = conn.cursor()
    current_revision = 0
    try:
        c.execute('SELECT revision FROM meta')
        current_revision = c.fetchone()[0]
    except sqlite3.OperationalError:
        pass
    print('DB REVISION:', current_revision)
    to_apply = MIGRATIONS[current_revision:REVISION]
    [c.execute(query) for migration in to_apply for query in migration]
    c.execute('DELETE FROM meta')
    c.execute('INSERT INTO meta VALUES (?)', (REVISION,))

    ## test data
    c.executemany('INSERT INTO project VALUES (?,?,?,?);', [
        ('hoa17', 'Hacken Open Air 2017', 'hoa17', 'hoa17_ro'),
        ('hoa18', 'Hacken Open Air 2018', 'hoa18', 'hoa18_ro')
    ])
    c.executemany('INSERT INTO user VALUES (?,?);', [
        ('foo', '$2b$12$n1wW384t91E3AOyAn1B5O.MS2b7.LS69whJPqBssSLnBmqZSK.wmi'),
    ])
    c.executemany('INSERT INTO issue VALUES (?,?,?,?,?,?);', [
        (1, 'hoa18', 'First Issue', 'something', 'foobar', 'pad_readonly'),
        (2, 'hoa18', 'Second Issue', 'else', 'pad_id', 'pad_readonly'),
        (3, 'hoa18', 'Third Issue', 'else', 'pad_id', 'pad_readonly'),
    ])
    c.executemany('INSERT INTO assignment VALUES (?,?);', [('foo', 1), ('bar', 1)])
    c.executemany('INSERT INTO issue_file VALUES (?,?,?,?,?,?);', [
        ('5ea553937c896326067c579d6d15c57f6061a30607b001bbb54f323dc76e6cbe', 2, 'foo', 'foo.png', 'image/png', 1567961768),
        ('12dd1713f4f6e4702a095f8dc81262ec7b7527cb738a9ac5208914b7864583b8', 2, 'bar', 'bar.txt', 'text/plain', 1567961761),
    ])
    c.executemany('INSERT INTO project_file VALUES (?,?,?,?,?,?);', [
        ('883995e738ff47b83cb47db08c07ca9abef4037f4d28632c5c60083e2fb9ec81', 'hoa18', 'foo', 'baz.pdf', 'application/pdf', 1567961790),
    ])

    conn.commit()
    return conn

