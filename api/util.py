import config
import jwt

class AuthenticationRequired(Exception):
    pass

def _verify_jwt(auth_header):
    if auth_header and auth_header.startswith('Bearer'):
        try:
            claim = jwt.decode(auth_header[7:], config.JWT_SECRET, config.JWT_ALG)
        except jwt.exceptions.DecodeError:
            return None
        if 'user' in claim:
            return claim
        else:
            return None
    else:
        return None

def exposed(_func=None, *, authentication=True):
    def wrapper(func):
        def wrapper_inner(*args, **kwargs):
            claim = _verify_jwt(args[1].headers.get('Authorization'))
            if claim is None and authentication:
                raise AuthenticationRequired()
            verified = (claim,)
            return func(*((args[0],) + verified + args[2:]), **kwargs)
        wrapper_inner._exposed = True
        return wrapper_inner
    return wrapper if _func is None else wrapper(_func)

