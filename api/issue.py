from .util import exposed

class Issue():
    def __init__(self, conn):
        self.conn = conn

    @exposed
    def create(self, auth, name):
        """
        Create a new issue. This method is still missing several parameters.

        :returns: An object representing the issue
        """
        pass

    @exposed
    def rename(self, auth, id, name):
        pass

    @exposed
    def archive(self, auth, id):
        pass

    @exposed
    def unarchive(self, auth, id):
        pass

    @exposed
    def remove(self, auth, id):
        """
        Delete the given issue. Should only be possible when archived.
        """
        pass

    @exposed
    def getFiles(self, auth, id):
        pass

    @exposed
    def deleteFile(self, auth, id, fileId):
        pass

    @exposed
    def listIssues(self, auth, projectId):
        pass

    @exposed
    def get(self, auth, id):
        pass

    @exposed
    def assign(self, auth, id):
        """
        Assign the authenticated user to the given issue.
        """
        pass

    @exposed
    def unassign(self, auth, id):
        """
        Unassign the authenticated user from the given issue.
        """
        pass

    @exposed
    def changeCategory(self, auth, id, category):
        pass
