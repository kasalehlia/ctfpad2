from .util import exposed

class Project():
    def __init__(self, conn):
        self.conn = conn

    @exposed
    def create(self, auth, name):
        """
        Create a new project.

        :returns: An object representing the event
        """
        pass

    @exposed
    def rename(self, auth, id, name):
        pass

    @exposed
    def archive(self, auth, id):
        pass

    @exposed
    def unarchive(self, auth, id):
        pass

    @exposed
    def remove(self, auth, id):
        """
        Delete the given project. Should only be possible when archived.
        """
        pass

    @exposed
    def listProjects(self, auth):
        pass
