import inspect
import json

import tornado

from .util import exposed, AuthenticationRequired
from .issue import Issue
from .project import Project
from .user import User

class JsonRpcHandler(tornado.web.RequestHandler):
    def initialize(self, api):
        self.api = api

    async def post(self):
        try:
            req = json.loads(self.request.body.decode())
        except json.decoder.JSONDecodeError:
            self.write('{"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error"}, "id": null}')
            return
        batch = isinstance(req, list)
        if not batch:
            req = [req]
        results = []
        for r in req:
            if r.get('jsonrpc') == '2.0' and 'method' in r and 'params' in r and 'id' in r:
                ans = {'jsonrpc': '2.0', 'id': r['id']}
                valid, result = await self.handle(r['method'], r['params'])
                if valid:
                    ans['result'] = result
                else:
                    ans['error'] = result
                results.append(ans)
            else:
                results.append({"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": r.get('id', None)})
        self.write(json.dumps(results if batch else results[0]))

    async def handle(self, method, params):
        if hasattr(self.api, method):
            fun = getattr(self.api, method)
            if callable(fun) and hasattr(fun, '_exposed') and fun._exposed is True:
                try:
                    result = fun(self.request, *params)
                    if inspect.iscoroutine(result):
                        result = await result
                except TypeError as e:
                    print(e)
                    return (False, dict(code=-32602, message='Invalid params'))
                except AuthenticationRequired:
                    return (False, dict(code=1, message='Insufficient Authentication'))
                return (True, result)
        return (False, dict(code=-32601, message='Method not found'))

