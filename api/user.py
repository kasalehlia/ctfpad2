import bcrypt
import jwt

import config

from .util import exposed

class User():
    def __init__(self, conn):
        self.conn = conn

    @exposed
    def changePassword(self, auth, password):
        """
        Change the password for the authenticated user.

        :param self: self
        :param auth: the claim from the JWT
        :param password: new password
        :returns: True if changing the password was sucessful, False otherwise
        """
        pass

    @exposed(authentication=False)
    def create(self, auth, user, password, secret):
        pass

    @exposed(authentication=False)
    def whoAmI(self, auth):
        return auth.get('user', None) if auth is not None else None

    @exposed
    def isAdmin(self, auth):
        pass

    @exposed
    def remove(self, auth):
        pass

    @exposed
    def promote(self, auth, user):
        pass

    @exposed
    def demote(self, auth, user):
        pass

    @exposed
    def listUsers(self, auth):
        pass

    @exposed(authentication=False)
    def getToken(self, auth, username, password):
        return jwt.encode({'user': username}, config.JWT_SECRET, config.JWT_ALG).decode()


