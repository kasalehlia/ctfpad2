import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {distinctUntilChanged, map} from 'rxjs/operators';

@Injectable()
export class LocalSettingsService {
  private readonly SIDEBAR_KEY = 'ctfpad2.sidebar';
  private readonly THEME_KEY = 'ctfpad2.theme';

  private sidebarOpenSubject = new BehaviorSubject<boolean>(localStorage.getItem(this.SIDEBAR_KEY) !== null);
  private themeSubject = new BehaviorSubject<string>(localStorage.getItem(this.THEME_KEY));

  public readonly sidebarOpen = this.sidebarOpenSubject.asObservable();
  public readonly theme = this.themeSubject.asObservable();

  constructor() {
    this.theme.subscribe((theme) => {
      (document.getElementById('theme-link') as HTMLLinkElement).href = `/assets/${theme || 'deeppurple-amber'}.css`;
    });
  }

  public toggleSidebar() {
    this.sidebarOpenSubject.next(!this.sidebarOpenSubject.getValue());
    if (this.sidebarOpenSubject.getValue()) {
      localStorage.setItem(this.SIDEBAR_KEY, 'yes');
    } else {
      localStorage.removeItem(this.SIDEBAR_KEY);
    }
  }

  public setTheme(themeId: string) {
    this.themeSubject.next(themeId);
    localStorage.setItem(this.THEME_KEY, themeId);
  }
}
