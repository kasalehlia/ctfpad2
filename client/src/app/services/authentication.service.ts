import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {distinctUntilChanged, map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
  private readonly KEY_NAME = 'ctfpad2.token';
  private initializedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly initialized = this.initializedSubject.asObservable();
  private tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(localStorage.getItem(this.KEY_NAME));
  public readonly token = this.tokenSubject.asObservable();
  private userSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public readonly user = this.userSubject.asObservable();
  public readonly authenticated: Observable<boolean> = combineLatest(this.tokenSubject, this.userSubject)
    .pipe(map(([token, user]): boolean => !!user && !!token), distinctUntilChanged());

  constructor(private http: HttpClient) {
    setTimeout(() => {
      this.token.subscribe((token) => {
        localStorage.setItem(this.KEY_NAME, token);
        setTimeout(() => {
          this.http.post<string>('/api/user', {method: 'whoAmI'}).toPromise().then((user) => {
            this.userSubject.next(user);
          }).catch(() => {
          }).finally(() => {
            this.initializedSubject.next(true);
          });
        }, 0);
      });
    }, 0);
  }

  public login(username: string, password: string): Promise<void> {
    return this.http.post<string>('/api/user', {method: 'getToken', params: [username, password]}).toPromise().then((token) => {
      if (token === null) {
        return Promise.reject();
      } else {
        this.tokenSubject.next(token);
      }
    });
  }

  public logout(): void {
    this.tokenSubject.next(null);
    this.userSubject.next(null);
  }
}
