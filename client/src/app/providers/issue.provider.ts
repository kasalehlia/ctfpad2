import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Issue} from '../models/issue.model';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {map} from 'rxjs/operators';

export interface GroupedIssues { [category: string]: Issue[]; }

@Injectable()
export class IssueProvider implements Resolve<Issue> {
  private issuesSubject = new BehaviorSubject<Issue[]>([]);
  public readonly issues = this.issuesSubject.asObservable();
  public readonly issuesByCategory: Observable<GroupedIssues> = this.issuesSubject.pipe(map(this.groupByCategory));
  private currentIssueSubject = new BehaviorSubject<Issue>(null);
  public readonly currentIssue = this.currentIssueSubject.asObservable();

  private loadingPromise: Promise<Issue[]>;

  constructor(private http: HttpClient) {
  }

  public loadEvent(id: string): Promise<Issue[]> {
    this.loadingPromise = this.http.post<Issue[]>('/api', {method: 'getIssues', params: [id]}).toPromise();
    this.loadingPromise.then((issues) => {
      this.loadingPromise = null;
      this.issuesSubject.next(issues);
    });
    return this.loadingPromise;
  }

  public getIssue(id: number): Promise<Issue> {
    if (!id) {
      return null;
    }
    const existing = this.issuesSubject.getValue().find((p) => p.id === id);
    if (existing) {
      return Promise.resolve(existing);
    } else if (this.loadingPromise) {
      return this.loadingPromise.then((is) => is.find((i) => i.id === id));
    } else {
      return Promise.reject();
    }
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Issue> | Promise<Issue> | Issue {
    const issueId = parseInt(route.paramMap.get('issue'), 10);
    if (isNaN(issueId)) {
      return Promise.reject();
    }
    const promise = this.getIssue(issueId);
    promise.then((issue) => this.currentIssueSubject.next(issue));
    return promise;
  }

  private groupByCategory(issues: Issue[]): GroupedIssues {
    const ret: GroupedIssues = {};
    issues.forEach((issue) => {
      if (!ret[issue.category]) { ret[issue.category] = []; }
      ret[issue.category].push(issue);
    });
    return ret;
  }
}
