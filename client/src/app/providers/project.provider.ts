import {Injectable} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {Project} from '../models/project.model';
import {HttpClient} from '@angular/common/http';
import {first, map} from 'rxjs/operators';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IssueProvider} from './issue.provider';

@Injectable()
export class ProjectProvider implements Resolve<Project> {
  private projectsSubject = new BehaviorSubject<Project[]>([]);
  public readonly projects = this.projectsSubject.asObservable();
  private currentProjectSubject = new BehaviorSubject<Project>(null);
  public readonly currentProject = this.currentProjectSubject.asObservable();
  private loaded = false;

  constructor(private authentication: AuthenticationService,
              private issueProv: IssueProvider,
              private http: HttpClient) {
    authentication.authenticated.subscribe((authenticated) => {
      if (authenticated) {
        this.http.post<Project[]>('/api/project', {method: 'listProjects'}).toPromise().then((projects) => {
          this.loaded = true;
          this.projectsSubject.next(projects || []);
        });
      } else {
        this.loaded = false;
      }
    });
  }

  public getProject(id: string): Promise<Project> {
    if (!id) {
      return null;
    }
    const existing = this.projectsSubject.getValue().find((p) => p.id === id);
    if (existing) {
      return Promise.resolve(existing);
    } else {
      return this.projectsSubject.pipe(first(() => this.loaded), map((ps) => ps.find((p) => p.id === id))).toPromise();
    }
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> | Promise<Project> | Project {
    const projectId = route.paramMap.get('project');
    if (projectId) {
      this.issueProv.loadEvent(projectId);
      const promise = this.getProject(projectId);
      promise.then((project) => this.currentProjectSubject.next(project));
      return promise;
    }
    return null;
  }
}
