import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatTabsModule,
  MatBadgeModule,
  MatSlideToggleModule,
  MatTableModule,
  MatListModule, MatAutocompleteModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  exports: [
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatMenuModule,
    MatSidenavModule,
    MatTabsModule,
    MatBadgeModule,
    MatSlideToggleModule,
    MatTableModule,
    MatListModule,
    MatAutocompleteModule,
  ]
})
export class MaterialMetaModule {}
