import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class JsonRpcInterceptor implements HttpInterceptor {
  private nextId = 1;
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith('/api/')) {
      const jsonRpcBody = {
        method: req.body.method,
        params: req.body.params || [],
        jsonrpc: '2.0',
        id: this.nextId,
      };
      this.nextId++;
      return next.handle(req.clone({body: jsonRpcBody})).pipe(map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.body.error) {
            throw event.body.error;
          }
          return event.clone({body: event.body.result});
        }
      }));
    } else {
      return next.handle(req);
    }
  }
}
