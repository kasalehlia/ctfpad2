import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import {Observable} from 'rxjs';
import {first, switchMap} from 'rxjs/operators';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  private static readonly noAuthNeeded = ['whoAmI', 'getToken'];

  private jwt: string;

  constructor(private authService: AuthenticationService) {
    this.authService.token.subscribe((token) => this.jwt = token);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.jwt || AuthenticationInterceptor.noAuthNeeded.indexOf(req.body.method) > -1) {
      return next.handle(req.clone({
        headers: req.headers.set('Authorization', `Bearer ${this.jwt}`)
      }));
    } else {
      return this.authService.authenticated.pipe(first((auth) => auth), switchMap(() => {
        return next.handle(req.clone({
          headers: req.headers.set('Authorization', `Bearer ${this.jwt}`)
        }));
      }));
    }
  }
}
