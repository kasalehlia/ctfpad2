import { BrowserModule } from '@angular/platform-browser';
import {Component, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import {AuthenticationService} from './services/authentication.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthenticationInterceptor} from './interceptors/authentication.interceptor';
import {JsonRpcInterceptor} from './interceptors/jsonrpc.interceptor';
import {ProjectProvider} from './providers/project.provider';
import {MainComponent} from './components/main/main.component';
import {ProjectChooserComponent} from './components/project-chooser/project-chooser.component';
import {ProjectComponent} from './components/project/project.component';
import {IssueProvider} from './providers/issue.provider';
import {IssueComponent} from './components/issue/issue.component';
import {FilesComponent} from './components/files/files.component';
import {PadComponent} from './components/pad/pad.component';
import {MaterialMetaModule} from './material-meta.module';
import {LoginComponent} from './components/login/login.component';
import {FormsModule} from '@angular/forms';
import {NavbarComponent} from './components/navbar/navbar.component';
import {LocalSettingsService} from './services/local-settings.service';
import {ProjectEditorComponent} from './components/project-editor/project-editor.component';
import {ThemeChooserComponent} from './components/theme-chooser/theme-chooser.component';

@Component({template: '<cp2-main></cp2-main>', selector: 'cp2-bootstrap'})
export class AppComponent {
  constructor(auth: AuthenticationService,
              projectProv: ProjectProvider) {
  }
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    LoginComponent,
    ProjectChooserComponent,
    ProjectComponent,
    ProjectEditorComponent,
    IssueComponent,
    FilesComponent,
    PadComponent,
    ThemeChooserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MaterialMetaModule,
  ],
  providers: [
    AuthenticationService,
    LocalSettingsService,
    ProjectProvider,
    IssueProvider,
    [
      {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true},
      {provide: HTTP_INTERCEPTORS, useClass: JsonRpcInterceptor, multi: true},
    ],
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

