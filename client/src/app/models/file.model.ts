export interface File {
  hash: string;
  user: string;
  title: string;
  mimetype: string;
  uploaded: number;
}
