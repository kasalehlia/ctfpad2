import {File} from './file.model';

export interface Issue {
  id: number;
  title: string;
  category: string;
  pad: string;
  assigned: string[];
  files: File[];
}
