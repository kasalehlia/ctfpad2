import {File} from './file.model';

export interface Project {
  id: string;
  title: string;
  pad: string;
  files: File[];
}
