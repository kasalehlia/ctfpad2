import {Component} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'cp2-main',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.scss']
})
export class MainComponent {
  private authenticated = false;
  private initialized = false;
  constructor(authentication: AuthenticationService) {
    authentication.authenticated.subscribe((authenticated) => this.authenticated = authenticated);
    authentication.initialized.subscribe((initialized) => this.initialized = initialized);
  }
}
