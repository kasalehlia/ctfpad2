import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Issue} from '../../models/issue.model';
import {File} from '../../models/file.model';
import {BehaviorSubject} from 'rxjs';
import {Project} from '../../models/project.model';

@Component({
  selector: 'cp2-files',
  templateUrl: 'files.component.html',
  styleUrls: ['files.component.scss']
})
export class FilesComponent implements OnInit, OnDestroy {
  private routeSub;

  private container: Project | Issue;

  files = new BehaviorSubject<File[]>([]);
  displayedColumns: string[] = ['title', 'user', 'mimetype', 'uploaded', 'hash', 'actions'];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.parent.data.subscribe((data: {issue: Issue} | {project: Project}) => {
      if ('project' in data) {
        this.files.next(data.project.files);
        this.container = data.project;
      } else {
        this.files.next(data.issue.files);
        this.container = data.issue;
      }
    });
  }

  copy(str: string): void {
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }
}
