import {Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../models/project.model';
import {ActivatedRoute, NavigationEnd, Router, RouterEvent} from '@angular/router';
import {GroupedIssues, IssueProvider} from '../../providers/issue.provider';
import {AuthenticationService} from '../../services/authentication.service';
import {LocalSettingsService} from '../../services/local-settings.service';
import {Issue} from '../../models/issue.model';
import {IssueComponent} from '../issue/issue.component';

@Component({
  selector: 'cp2-project',
  templateUrl: 'project.component.html',
  styleUrls: ['project.component.scss']
})
export class ProjectComponent implements OnInit, OnDestroy {
  private routeSub;
  private routerSub;
  private issueSub;
  private authSub;
  private sidenavSub;

  public project: Project = null;
  public issue: Issue = null;
  public issues: GroupedIssues = {};
  public user: string;

  private lastChildRoute: ActivatedRoute;

  sidenavOpened = true;

  objectKeys = Object.keys;

  constructor(public route: ActivatedRoute,
              private router: Router,
              private issueProv: IssueProvider,
              private sidenav: LocalSettingsService,
              private authentication: AuthenticationService) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.data.subscribe((data: {project: Project}) => this.project = data.project);
    this.issueSub = this.issueProv.issuesByCategory.subscribe((issues) => this.issues = issues);
    this.authSub = this.authentication.user.subscribe((user) => this.user = user);
    this.sidenavSub = this.sidenav.sidebarOpen.subscribe((opened) => this.sidenavOpened = opened);
    this.routerSub = this.router.events.subscribe(this.getIssue.bind(this));
    this.getIssue(null);
  }

  private getIssue(event: RouterEvent): void {
    if (event instanceof NavigationEnd || event === null) {
      if (this.lastChildRoute !== this.route.firstChild) {
        if (this.route.firstChild && this.route.firstChild.component === IssueComponent) {
          this.route.firstChild.data.subscribe((data: {issue: Issue}) => this.issue = data.issue);
        } else if (this.issue !== null) {
          this.issue = null;
        }
        this.lastChildRoute = this.route.firstChild;
      }
    }
  }

  private setAssigned(issue: Issue, assigned: boolean): void {
    const index = issue.assigned.indexOf(this.user);
    if (assigned && index === -1) {
      issue.assigned.push(this.user);
    } else if (!assigned && index > -1) {
      issue.assigned.splice(index, 1);
    }
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.routerSub.unsubscribe();
    this.issueSub.unsubscribe();
    this.authSub.unsubscribe();
    this.sidenavSub.unsubscribe();
  }
}
