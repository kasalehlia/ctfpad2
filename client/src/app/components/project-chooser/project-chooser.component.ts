import {Component, OnDestroy} from '@angular/core';
import {Project} from '../../models/project.model';
import {ProjectProvider} from '../../providers/project.provider';

@Component({
  selector: 'cp2-project-chooser',
  templateUrl: 'project-chooser.component.html',
  styleUrls: ['project-chooser.component.scss']
})
export class ProjectChooserComponent implements OnDestroy {
  private projectsSub;

  public projects: Project[] = [];

  constructor(private projectProv: ProjectProvider) {
    this.projectsSub = this.projectProv.projects.subscribe((projects) => this.projects = projects);
  }

  ngOnDestroy(): void {
    this.projectsSub.unsubscribe();
  }
}
