import {Component} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'cp2-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent {
  username: string;
  password: string;
  error: string;

  constructor(private authentication: AuthenticationService) {
  }

  login(): void {
    this.authentication.login(this.username, this.password).catch(() => this.error = 'Wrong username or password.');
  }
}
