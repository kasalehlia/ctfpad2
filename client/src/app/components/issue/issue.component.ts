import {Component, OnDestroy} from '@angular/core';
import {Project} from '../../models/project.model';
import {ProjectProvider} from '../../providers/project.provider';
import {ActivatedRoute} from '@angular/router';
import {IssueProvider} from '../../providers/issue.provider';
import {Issue} from '../../models/issue.model';

@Component({
  selector: 'cp2-issue',
  templateUrl: 'issue.component.html',
  styleUrls: ['issue.component.scss']
})
export class IssueComponent implements OnDestroy {
  public issue: Issue = null;
  constructor(private route: ActivatedRoute) {
    this.route.data.subscribe((data: {issue: Issue}) => this.issue = data.issue);
  }

  ngOnDestroy(): void {
  }
}
