import {Component, OnDestroy, SecurityContext} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Issue} from '../../models/issue.model';
import {AuthenticationService} from '../../services/authentication.service';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {combineLatest} from 'rxjs/operators';
import {Project} from '../../models/project.model';

@Component({
  selector: 'cp2-issue-files',
  templateUrl: 'pad.component.html',
  styleUrls: ['pad.component.scss']
})
export class PadComponent implements OnDestroy {
  private combinedSub;

  public container: Project | Issue = null;
  public username: string;
  public padUrl: SafeUrl;

  constructor(private route: ActivatedRoute,
              private authentication: AuthenticationService,
              private sanitizer: DomSanitizer) {
    this.combinedSub = this.route.parent.data.pipe(combineLatest(this.authentication.user))
      .subscribe(([data, user]: [{issue: Issue} | {project: Project}, string]) => {
        const container: Project | Issue = ('project' in data) ? data.project : data.issue;
        if (container && (container.pad !== (this.container ? this.container.pad : null) || user !== this.username)) {
          this.padUrl = this.sanitize(`/pad/p/${container.pad}?userName=${user}`);
        }
        this.container = container;
        this.username = user;
    });
  }

  sanitize(str: string): SafeUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.sanitizer.sanitize(SecurityContext.URL, str));
  }

  ngOnDestroy(): void {
    this.combinedSub.unsubscribe();
  }
}
