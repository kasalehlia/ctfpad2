import {Component} from '@angular/core';
import {LocalSettingsService} from '../../services/local-settings.service';

export interface Theme {
  name: string;
  id: string;
  primaryColor: string;
  backgroundColor: string;
}

@Component({
  selector: 'cp2-theme-chooser',
  templateUrl: 'theme-chooser.component.html',
  styleUrls: ['theme-chooser.component.scss']
})
export class ThemeChooserComponent {
  public themes: Theme[] = [
    {name: 'Deep Purple', id: 'deeppurple-amber', primaryColor: '#673ab7', backgroundColor: '#fafafa'},
    {name: 'Indigo', id: 'indigo-pink', primaryColor: '#3f51b5', backgroundColor: '#fafafa'},
    {name: 'Pink (dark)', id: 'pink-bluegrey', primaryColor: '#e91e63', backgroundColor: '#303030'},
    {name: 'Purple (dark)', id: 'purple-green', primaryColor: '#9c27b0', backgroundColor: '#303030'},
  ];

  constructor(private localSettings: LocalSettingsService) {
  }

  public setTheme(theme: Theme) {
    this.localSettings.setTheme(theme ? theme.id : this.themes[0].id);
  }
}
