import {Component} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Project} from '../../models/project.model';
import {ProjectProvider} from '../../providers/project.provider';
import {Issue} from '../../models/issue.model';
import {GroupedIssues, IssueProvider} from '../../providers/issue.provider';
import {LocalSettingsService} from '../../services/local-settings.service';

@Component({
  selector: 'cp2-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.scss']
})
export class NavbarComponent {
  private user: string;
  private project: Project;
  private projects: Project[] = [];
  private issue: Issue;
  private issues: GroupedIssues;

  objectKeys = Object.keys;

  constructor(private authentication: AuthenticationService,
              private projectProv: ProjectProvider,
              private issueProv: IssueProvider,
              public sidenav: LocalSettingsService) {
    this.authentication.user.subscribe((user) => this.user = user);
    this.projectProv.currentProject.subscribe((project) => this.project = project);
    this.projectProv.projects.subscribe((projects) => this.projects = projects);
    this.issueProv.currentIssue.subscribe((issue) => this.issue = issue);
    this.issueProv.issuesByCategory.subscribe((issues) => this.issues = issues);
  }

  logout(): void {
    this.authentication.logout();
  }
}
