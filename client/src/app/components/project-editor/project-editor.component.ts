import {Component, OnDestroy, OnInit} from '@angular/core';
import {Project} from '../../models/project.model';
import {ActivatedRoute} from '@angular/router';
import {IssueProvider} from '../../providers/issue.provider';
import {Issue} from '../../models/issue.model';

@Component({
  selector: 'cp2-project-editor',
  templateUrl: 'project-editor.component.html',
  styleUrls: ['project-editor.component.scss']
})
export class ProjectEditorComponent implements OnInit, OnDestroy {
  private routeSub;
  private issueSub;

  public project: Project = null;
  public issues: Issue[] = [];

  editingTitle: string;

  constructor(private route: ActivatedRoute,
              private issueProv: IssueProvider) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.parent.data.subscribe((data: {project: Project}) => this.project = data.project);
    this.issueSub = this.issueProv.issues.subscribe((issues) => this.issues = issues);
  }

  get categories(): string[] {
    // FIXME find a more performant solution
    const cats = [];
    this.issues.forEach((issue) => {
      if (issue.category && !cats.includes(issue.category)) { cats.push(issue.category); }
    });
    return cats;
  }

  startEditingTitle(): void {
    this.editingTitle = this.project.title;
  }

  stopEditingTitle(save: boolean): void {
    if (save) {
      // TODO send to server
      this.project.title = this.editingTitle;
    }
    this.editingTitle = null;
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
    this.issueSub.unsubscribe();
  }
}
