import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProjectChooserComponent} from './components/project-chooser/project-chooser.component';
import {ProjectComponent} from './components/project/project.component';
import {IssueComponent} from './components/issue/issue.component';
import {FilesComponent} from './components/files/files.component';
import {PadComponent} from './components/pad/pad.component';
import {IssueProvider} from './providers/issue.provider';
import {ProjectProvider} from './providers/project.provider';
import {ProjectEditorComponent} from './components/project-editor/project-editor.component';


const routes: Routes = [
  {path: '', component: ProjectChooserComponent},
  {path: ':project', component: ProjectComponent, resolve: {project: ProjectProvider}, children: [
      {path: 'edit', component: ProjectEditorComponent},
      {path: 'pad', component: PadComponent},
      {path: 'files', component: FilesComponent},
      {path: ':issue', component: IssueComponent, resolve: {issue: IssueProvider}, children: [
          {path: 'pad', component: PadComponent},
          {path: 'files', component: FilesComponent},
        ]},
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
