import bcrypt
import jwt
import re

import etherpad
from database import MODELS as models

JWT_SECRET = 'iShieBo2eef7Hur4ohY9ohpiaeChie4o'
JWT_ALG = 'HS256'

EMPTY_PW = b'$2b$12$vTmFcaDkExsIuCI2XbjtzuQQY.JpkjH2bKi/gsjRl1pUH4wPvJbI.'

class AuthenticationRequired(Exception):
    pass

def exposed(_func=None, *, authentication=True):
    def wrapper(func):
        def wrapper_inner(*args, **kwargs):
            verified = tuple()
            if authentication:
                auth = args[1].headers.get('Authorization')
                if auth and auth.startswith('Bearer'):
                    try:
                        claim = jwt.decode(auth[7:], JWT_SECRET, JWT_ALG)
                    except jwt.exceptions.DecodeError:
                        raise AuthenticationRequired()
                    if 'user' in claim:
                        verified = (claim,)
                    else:
                        raise AuthenticationRequired()
                else:
                    raise AuthenticationRequired()
            return func(*((args[0],) + verified + args[2:]), **kwargs)
        wrapper_inner._exposed = True
        return wrapper_inner
    return wrapper if _func is None else wrapper(_func)

class Api():
    def __init__(self, conn):
        self.conn = conn

    def map(self, model, results):
        return [dict(zip(models[model], result)) for result in results]

    @exposed(authentication=False)
    def getToken(self, user, password):
        res = self.conn.execute('SELECT pwhash FROM user WHERE name = ?', (user,)).fetchone()
        if res is not None and bcrypt.checkpw(password.encode(), res[0].encode()):
            return jwt.encode({'user': user}, JWT_SECRET, JWT_ALG).decode()
        elif res is None:
            bcrypt.checkpw('foo', EMPTY_PW) # do a pwcheck to mtigate timing attacks
        return None

    @exposed
    def whoami(self, auth):
        return auth.get('user', None)

    @exposed
    def getProjects(self, auth):
        projects = self.map('project', self.conn.execute('SELECT * FROM project;').fetchall())
        for project in projects:
            del project['pad_ro']
            files = self.conn.execute('SELECT * FROM project_file WHERE project = ?', (project['id'],))
            project['files'] = self.map('project_file', files.fetchall())
        return projects


    @exposed
    def getIssues(self, auth, project):
        issues = self.map('issue', self.conn.execute('SELECT * FROM issue WHERE project = ?', (project,)).fetchall())
        for issue in issues:
            del issue['pad_ro']
            assigns = self.conn.execute('SELECT user FROM assignment WHERE issue = ?', (issue['id'],))
            issue['assigned'] = list(map(lambda a: a[0], assigns.fetchall()))
            files = self.conn.execute('SELECT * FROM issue_file WHERE issue = ?', (issue['id'],))
            issue['files'] = self.map('issue_file', files.fetchall())
        return issues

    @exposed(authentication=False)
    async def padContent(self, pad_id):
        return await etherpad.pad_content(pad_id)

    @exposed
    async def searchInPads(self, auth, text):
        res = self.conn.execute('SELECT id,project,pad FROM issue').fetchall()
        ret = {}
        for issue_id, project_id, pad_id in res:
            content = await etherpad.pad_content(pad_id)
            if text.lower() in content.lower():
                if project_id not in ret: ret[project_id] = dict()
                ret[project_id][pad_id] = [content[max(0,hit.start()-30):hit.end()+30] for hit in re.finditer(text.lower(), content.lower())]
        return ret


