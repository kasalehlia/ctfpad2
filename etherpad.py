import json
from urllib.parse import urlencode

from tornado import httpclient

EP_BASE = 'http://127.0.0.1:9001'
API_BASE = EP_BASE+'/api/1/'

apikey = None
with open('etherpad/APIKEY.txt', 'r') as f:
    apikey = f.read()

class EtherpadApiException(Exception):
    pass

async def _request(method, **kwargs):
    kwargs['apikey'] = apikey
    qs = urlencode(kwargs)
    client = httpclient.AsyncHTTPClient()
    res = await client.fetch(API_BASE+method+'?'+qs)
    ans = json.loads(res.body.decode())
    if ans['code'] > 0:
        raise EtherpadApiException(ans['message'])
    return ans['data']

async def pad_content(pad_id):
    """fetches content of a pad from the etherpad instance"""
    return (await _request('getText', padID=pad_id))['text']
