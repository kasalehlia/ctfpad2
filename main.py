import tornado.ioloop
import tornado.web

import database
import api

import etherpad

if __name__ == "__main__":
    db_conn = database.init(':memory:')

    application = tornado.web.Application([
        (r"/api/user", api.JsonRpcHandler, dict(api=api.User(db_conn))),
        (r"/api/project", api.JsonRpcHandler, dict(api=api.Project(db_conn))),
        (r"/api/issue", api.JsonRpcHandler, dict(api=api.Issue(db_conn))),
        #(r"/(.*)", tornado.web.StaticFileHandler, dict(path='static/', default_filename='index.html')),
    ])
    application.listen(8008)
    tornado.ioloop.IOLoop.current().start()

